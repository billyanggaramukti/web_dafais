<?php require "../connections/config.php"; ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bootstrap Material Admin by Bootstrapious.com</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../component/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../component/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../component/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../component/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../component/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="../component/img/favicon.ico">
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <div class="page login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <div class="row">
            <!-- Logo & Information Panel-->
            <div class="col-lg-6">
              <div class="info d-flex align-items-center">
                <div class="content text-center">
                  <div class="logo">
                    <?php $query = @sqlsrv_query($dbconnect, "select * from WebSettingDasar where NamaSetting = 'Logo'") or die( print_r( sqlsrv_errors(), true));
				    while($data = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ 
					if($data['Value'] == null OR $data['Value'] === "") {
							echo '<img src="../images/assets/thumb_no-image.png" class="img img-responsive img-thumbnail" alt="Image">';
						} else {
							echo '<img src="../images/assets/thumb_'.$data["Value"].'" class="img img-responsive img-thumbnail" alt="Image">';
						}
					}
					?>
                  </div><br>
                  <span><strong>DANA FAMILY ISLAMI</strong></span>
                </div>
              </div>
            </div>
            <!-- Form Panel    -->
            <div class="col-lg-6 bg-white">
              <div class="form d-flex align-items-center">
                <div class="content">
                  <form method="post" class="form-validate">
                    <div class="form-group">
                      <input id="login-username" type="text" name="_username" required data-msg="Masukkan Username" class="input-material" autocomplete="off" required>
                      <label for="login-username" class="label-material">Username</label>
                    </div>
                    <div class="form-group">
                      <input id="login-password" type="password" name="_password" required data-msg="Masukkan Password" class="input-material" autocomplete="off" required>
                      <label for="login-password" class="label-material">Password</label>
                    </div>
					<div class="col-lg-12"><img src="../library/captcha/captcha_login.php" alt="gambar" /><br><br>
					<div class="form-group">
						<input id="login-captcha" type="text" name="_captcha_login" required data-msg="Masukkan Captcha" class="input-material" autocomplete="off" required>
						<label for="login-captcha" class="label-material">Captcha</label>
					</div>
					<button type="submit" name="ProsesLogin" class="btn btn-primary">Masuk</button>
                    <!-- This should be submit button but I replaced it with <a> for demo purposes-->
                  </form><!-- <a href="#" class="forgot-pass">Forgot Password?</a><br><small>Do not have an account? </small><a href="register.html" class="signup">Signup</a> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
   
    </div>
    <!-- JavaScript files-->
    <script src="../component/vendor/jquery/jquery.min.js"></script>
    <script src="../component/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../component/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../component/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="../component/vendor/chart.js/Chart.min.js"></script>
    <script src="../component/vendor/jquery-validation/jquery.validate.min.js"></script>
    <!-- Main File-->
    <script src="../component/js/front.js"></script>
	
	<?php
	@session_start();
	if(isset($_POST['ProsesLogin'])){
		if(empty($_POST['_username']) || empty ($_POST ['_password'])) {
			echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Username atau Password Kosong ", type: "error" },
				  function () { window.location.href = "login.php"; });
				  </script>';
			@session_destroy();
		}
		else{		
		// Variabel username dan password
		$username = @stripslashes(@$_POST['_username']);
		$password = @stripslashes(md5(@$_POST['_password']));
		$waktu    = time()+25200; //(GMT+7)
		$expired  = 600;
		
		// Mencegah MySQL injection dan XSS
		$check_user = @htmlspecialchars(addslashes($username), ENT_QUOTES);
		$check_pass = @htmlspecialchars(addslashes($password), ENT_QUOTES);
		
		// SQL query untuk memeriksa apakah user terdapat di database?
		$query = @sqlsrv_query($dbconnect, "select * from WebMstUser where UserName = '$check_user' AND Password = '$check_pass' AND IsAktif='1'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
		while($cari = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)) { $kode = @base64_encode($cari['KodeUser']); }
		$rows = @sqlsrv_num_rows($query);
		if(@$_SESSION["_captcha_login"] === $_POST["_captcha_login"]){
			if($rows === 0){
				echo '<script type="text/javascript">
					  sweetAlert({ title: "Login Gagal!", text: " Username Dan Password Salah Atau User Tidak Aktif", type: "error" },
					  function () { window.location.href = "login.php"; }); </script>';
				@session_destroy();
			} 
			else{
				@$_SESSION['_userlogin'] = $kode; // Membuat Sesi/session
				/* $_SESSION['_timeout']= $waktu + $expired; // Membuat Sesi Waktu */
				echo '<script type="text/javascript">
					  sweetAlert({ title: "Login Sukses!", text: " Anda Berhasil Login ", type: "success" },
					  function () { window.location.href = "../administrator/default.php"; }); </script>';
			}
			@sqlsrv_close(); // Menutup koneksi
		}else{
			echo '<script type="text/javascript">sweetAlert("Maaf!", " Captcha Anda Salah ", "error"); </script>';
		}
		
		}
	}
	?>
	
  </body>
</html>