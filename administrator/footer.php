<!-- Footer Section -->
<footer class="main-footer">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-6"><p>PT. Dana Family Islami &copy; 2018</p></div>
            <div class="col-sm-6 text-right"><p>Design by <a href="https://afindo-inf.com" class="external">Afindo Informatika</a></p>
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
			</div>
		</div>
	</div>
</footer>