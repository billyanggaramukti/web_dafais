<?php require "../connections/config.php"; require "../library/tgl-indo.php"; include "akses.php";

// get variable POST
$id = isset($_GET['id']) ? htmlspecialchars(base64_decode(@$_GET['id'])) : null ; 
$page = isset($_GET['page']) ? @$_GET['page'] : null ; 
$keyword = isset($_REQUEST['_keyword']) ? @$_REQUEST['_keyword'] : null ; 
$aktif = isset($_REQUEST['_aktif']) ? @$_REQUEST['_aktif'] : null ; 

// hak akses
$nopage = 4; require "../library/lock-menu.php";
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include "head.php"; ?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../component/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../component/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../component/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../component/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../component/css/custom.css">
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	<!-- Upload Image -->
	<link rel="stylesheet" href="../library/upload-image/upload-img.css" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <?php if($id !== null AND $page === "_nonaktif"){
		// non aktif bukutamu
		$query = @sqlsrv_query($dbconnect, "UPDATE WebBukuTamu SET IsAktif = '0' WHERE KodeBuku = '".$id."'") or die( print_r( sqlsrv_errors(), true));
		echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Non Aktif Data Berhasil ", type: "success" },
		function () { window.location.href = "bukutamu.php"; }); </script>';
		
	} elseif($id !== null AND $page === "_aktif"){
		// aktif bukutamu
		$query = @sqlsrv_query($dbconnect, "UPDATE WebBukuTamu SET IsAktif = '1' WHERE KodeBuku = '".$id."'") or die( print_r( sqlsrv_errors(), true));
		echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Aktif Data Berhasil ", type: "success" },
		function () { window.location.href = "bukutamu.php"; }); </script>';
		
	} elseif($id !== null AND $page === "_delete_testimoni"){
		// delete testimoni
		$delete = @sqlsrv_query($dbconnect, "DELETE from WebBukuTamu WHERE KodeBuku = '".$id."'") or die( print_r( sqlsrv_errors(), true));
		if($delete){
			echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Delete Data Sukses ", type: "success" },
			function () { window.location.href = "bukutamu.php"; }); </script>';
		} else {
			echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Delete Data Gagal ", type: "error" },
			function () { window.location.href = "bukutamu.php"; }); </script>';
		}
		
	} ?>
	
    <div class="page">
      <!-- Main Navbar-->
	  <?php include "header.php"; ?>
	  <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include "sidebar.php"; ?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Testimoni Pengunjung</h2>
			</div>
          </header>
		  
		  <!-- Modal Detail Data -->
          <div id="detailData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
            <div role="document" class="modal-dialog">
              <div class="modal-content">
                  <div class="fetched-data"></div>
              </div>
            </div>
          </div>
		  
          <!-- Dashboard Counts Section-->
          <section class="dashboard-counts no-padding-bottom">
            <div class="container-fluid">
              <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">List Data</h3>
                    </div>
                    <div class="card-body">
					  <form method="post" action="">
						 <div class="form-group input-group">
						    <div class="col-lg-6">
							   <?php if(@$_REQUEST['aktif'] == null){
									echo '<input type="checkbox" id="option" name="aktif" value="1" />';
								} else {
									echo '<input type="checkbox" id="option" name="aktif" value="1" checked="checked" />';
								} 
							   ?>
							   <label for="option">&nbsp;Tampilkan Data Non Aktif</label>
							</div>
							<div class="col-lg-6">
							   <div class="input-group">
								  <input type="text" name="keyword" class="form-control" placeholder="Nama Pengunjung" value="<?php echo htmlspecialchars(@$_REQUEST['keyword']); ?>" autocomplete="off">
								  <div class="input-group-append"><button type="submit" class="btn btn-primary">Cari</button></div>
							   </div>
							</div>
						 </div>
					  </form>
						
					  <div class="table-responsive">                       
                        <table class="table table-striped table-hover">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Detail</th>
                              <th>Deskripsi</th>
                              <th>Status</th>
                              <th>Aksi</th>
                            </tr>
                          </thead>
						  
						  <?php 
						  //procedure paging
						  require "../connections/config.php";
						  include "../library/pagination.php";
						  $rowsPerPage = 20;
						  // mengatur variabel reload dan sql
						  $aktif = @$_REQUEST['aktif'];
						  if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
							 // jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)
							 $keyword = htmlspecialchars(@$_REQUEST['keyword']);
							 if(@$aktif == null){
								$reload = "mst-slider.php?pagination=true&keyword=$keyword";
								$sql = "SELECT * FROM WebBukuTamu WHERE Author LIKE '%$keyword%' AND IsAktif = '1' ORDER BY KodeBuku DESC";
							 } else {
							    $reload = "mst-slider.php?pagination=true&keyword=$keyword&aktif=$aktif";
								$sql = "SELECT * FROM WebBukuTamu WHERE Author LIKE '%$keyword%' AND (IsAktif = '0' OR IsAktif is null) ORDER BY KodeBuku DESC";
							 }
							 $result = @sqlsrv_query($dbconnect, $sql, array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
						  } else {
							 // jika tidak ada kata kunci, pencarian pakai ini
							 if(@$aktif == null){
								$reload = "mst-slider.php?pagination=true";
								$sql = "SELECT * FROM WebBukuTamu WHERE IsAktif = '1' ORDER BY KodeBuku DESC";
							 } else {
								$reload = "mst-slider.php?pagination=true&aktif=$aktif";
								$sql = "SELECT * FROM WebBukuTamu WHERE (IsAktif = '0' OR IsAktif is null) ORDER BY KodeBuku DESC";
							 }
							 $result = @sqlsrv_query($dbconnect, $sql, array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
						  }
						  
						  require "../library/get-page.php";
						  echo "<tbody>";
						  $pageNum = isset($_GET['pageNum']) ? $_GET['pageNum'] : 1;
						  $no_urut = ($pageNum-1)*$rowsPerPage; 
						  $page = getPage($result, $pageNum, $rowsPerPage);

						  foreach($page as $row){ ?>
						  
                            <tr class="text-left">
                              <td width="5%"><?php echo ++$no_urut; ?></th>
                              <td width="25%"><?php echo "<strong>".ucwords($row[2])."</strong><br>".ucwords($row[3])."<br>".TanggalIndo(DATE_FORMAT($row[1],'Y-m-d')); ?></td>
                              <td><?php echo ucwords(substr($row[4],0,100))." ... "; ?></td>
                              <td width="10%"><?php if($row[5] === 1) {
								echo '<a href="bukutamu.php?page=_nonaktif&id='.htmlspecialchars(base64_encode($row[0])).'" class="confirm-delete">
										<span class="btn btn-sm btn-success" title="Non Aktifkan">Aktif</span>
									  </a>';  
							  } else {
								echo '<a href="bukutamu.php?page=_aktif&id='.htmlspecialchars(base64_encode($row[0])).'" class="confirm-delete">
										<span class="btn btn-sm btn-danger" title="Aktifkan">Non Aktif</span>
									  </a>';   
							  } ?>
							  </td>
							  <?php if(@$_REQUEST['aktif'] == null){
								echo '<td width="10%">';
							  } else {
								echo '<td width="15%">';  
							  } ?>
								<a href="#detailData" data-toggle="modal" data-id="<?php echo htmlspecialchars(base64_encode($row[0])); ?>">
									<span class="btn btn-sm btn-info" title="Lihat Data">View</span>
								</a>
								
								<?php if(@$_REQUEST['aktif'] == null){
									echo '';
								} else {
									echo '<a href="bukutamu.php?page=_delete_testimoni&id='.htmlspecialchars(base64_encode($row[0])).'" class="confirm-delete">
										  <span class="btn btn-sm btn-danger" title="Hapus Data">Delete</span></a>';
								} 
								?>
							  </td>
                            </tr>
                          
						  <?php } 
						  echo "</tbody>";
						  			
						  $rowsReturned = sqlsrv_num_rows($result);
						  if($rowsReturned === false) {
							 die( print_r( sqlsrv_errors(), true));
						  } elseif($rowsReturned == 0) {
							 echo '<tr class="text-center"><td colspan="5"><br><h5>Tidak Ada Data</h5><br></td></tr>';
							 echo '</table></div>';
						  } else {     
							 $numOfPages = ceil($rowsReturned/$rowsPerPage);
							 echo '</table></div><br>';
							 echo '<div class="text-center">'.paginate_one($reload, $pageNum, $numOfPages).'</div>';
						  } ?>
						
                    </div>
                  </div>
              </div>
            </div>
          </section>
          
          <!-- Page Footer-->
          <?php include "footer.php"; ?>
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../component/vendor/jquery/jquery.min.js"></script>
    <script src="../component/vendor/popper.js/umd/popper.min.js"></script>
    <script src="../component/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../component/vendor/jquery.cookie/jquery.cookie.js"></script>
    <script src="../component/vendor/chart.js/Chart.min.js"></script>
    <script src="../component/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../component/js/charts-home.js"></script>
    <!-- Main File-->
    <script src="../component/js/front.js"></script>
	
	<!-- No Back Function -->
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<!-- Link to Modal Detail Data -->
	<script type="text/javascript">
    $(document).ready(function(){
        $('#detailData').on('show.bs.modal', function(e) {
			var rowid = $(e.relatedTarget).data('id');
            $.ajax({
				type : 'GET', url : 'detail-data.php?page=<?php echo htmlspecialchars(base64_encode("_detail_testimoni")); ?>', data : 'rowid='+ rowid,
				success : function(data){
					$('.fetched-data').html(data);
				}
            });
         });
    });
	</script>
	
	<!-- Option to Delete Data -->
	<script type="text/javascript">
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Aktifkan, Non Aktifkan atau Hapus Data Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script>
	
  </body>
</html>