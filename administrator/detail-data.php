<?php 
require "../connections/config.php"; 
require "../library/tgl-indo.php"; 
date_default_timezone_set('Asia/Jakarta'); 

// get variable POST
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : null ; 
$id = isset($_GET['rowid']) ? base64_decode($_GET['rowid']) : null ; 

?>

<div class="modal-header">
	<h4 id="exampleModalLabel" class="modal-title">Detail Data</h4>
	<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span class="fa fa-close"aria-hidden="true"></span></button>
</div>

<!-- Modal Slider -->
<?php if($page === "_detail_slider") { 
	$query = @sqlsrv_query($dbconnect, "select * from WebMstSlider where KodeSlider = '".$id."'") or die( print_r( sqlsrv_errors(), true));
	while($data = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ $judul = $data["Judul"]; $desc = $data["Deskripsi"]; $gbr = $data["Gambar"]; }
?>
<div class="modal-body">
	<div class="form-group">
		<?php if($gbr == null OR $gbr === ""){
			echo '<img src="../images/sliders/no-image.png" class="img img-responsive img-thumbnail" alt="Image">';
		} else {
			echo '<img src="../images/sliders/'.$gbr.'" class="img img-responsive img-thumbnail" alt="Image">';
		} ?>
	</div>
	<div class="form-group">
		<dl>
			<dt>Judul</dt><dd><?php echo ucwords($judul); ?></dd><hr>
			<dt>Deskripsi</dt><dd><?php echo ucwords($desc); ?></dd>
		</dl>
	</div>
</div>

<?php } elseif($page === "_detail_post") { 
	$query = @sqlsrv_query($dbconnect, "select * from WebMstPost where KodePost = '".$id."'") or die( print_r( sqlsrv_errors(), true));
	while($data = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ $judul = $data["Judul"]; $desc = $data["Deskripsi"]; $tgl = $data["Tanggal"]; $author = $data["Author"]; $gbr = $data["Gambar"]; }
?>
<div class="modal-body">
	<div class="form-group">
		<?php if($gbr == null OR $gbr === ""){
			echo '<img src="../images/posts/no-image.png" class="img img-responsive img-thumbnail" alt="Image">';
		} else {
			echo '<img src="../images/posts/'.$gbr.'" class="img img-responsive img-thumbnail" alt="Image">';
		} ?>
	</div>
	<div class="form-group">
		<dl>
			<dt>Judul</dt><dd><?php echo ucwords($judul); ?></dd><hr>
			<dt>Deskripsi</dt><dd><?php echo TanggalIndo(DATE_FORMAT($tgl,'Y-m-d'))." oleh ".ucwords($author)."<hr>".ucwords($desc); ?></dd>
		</dl>
	</div>
</div>

<?php } elseif($page === "_detail_testimoni") { 
	$query = @sqlsrv_query($dbconnect, "select * from WebBukuTamu where KodeBuku = '".$id."'") or die( print_r( sqlsrv_errors(), true));
	while($data = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ $tgl = $data["Tanggal"]; $author = $data["Author"]; $desc = $data["Deskripsi"]; $email = $data["Email"]; }
?>
<div class="modal-body">
	<div class="form-group">
		<dl>
			<dt>Tanggal</dt><dd><?php echo TanggalIndo(DATE_FORMAT($tgl,'Y-m-d')); ?></dd><hr>
			<dt>Detail</dt><dd><?php echo ucwords($author)."<br>".$email; ?></dd><hr>
			<dt>Deskripsi</dt><dd><?php echo ucwords($desc); ?></dd>
		</dl>
	</div>
</div>
<?php } elseif($page === "_detail_user") { 
	$query = @sqlsrv_query($dbconnect, "select * from WebMstUser where KodeUser = '".$id."'") or die( print_r( sqlsrv_errors(), true));
	while($data = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ $nama = $data['NamaUser']; $alamat = $data['Alamat']; $telp = $data['NoTelp']; $email = $data['Email']; $gambar = $data['Gambar']; }
?>
<div class="modal-body">
	<div class="form-group">
		<dl>
			<?php if($gambar == null OR $gambar === '') {
				echo '<dd><div class="col-lg-4 avatar"><img src="../component/img/avatar-user.jpg" alt="Image" class="img-fluid rounded-circle"></div><dd><hr>';
			} else {
				echo '<dd><div class="avatar"><img src="../images/users/thumb_'.$gambar.'" alt="Image" class="img-fluid rounded-circle"></div><dd><hr>';
			} ?>
			<dt>Nama User</dt><dd><?php echo ucwords($nama); ?></dd><hr>
			<dt>Informasi</dt><dd><?php echo "Alamat : <br>".ucwords($alamat)."<br>Kontak Person : <br>".$telp."<br>Email : <br>".$email; ?></dd>
		</dl>
	</div>
</div>
<?php } ?>

<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-secondary">Keluar</button>
</div>