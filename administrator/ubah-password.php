<?php require "../connections/config.php"; include "akses.php"; ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include "head.php"; ?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../component/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../component/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../component/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../component/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../component/css/custom.css">
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	<!-- Upload Image -->
	<link rel="stylesheet" href="../library/upload-image/upload-img.css" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <?php if(isset($_POST['_save_pass'])){
		// mengambil POST update user
		$_pass1 = isset($_POST['_pass1']) ? htmlspecialchars($_POST['_pass1'], ENT_QUOTES) : null ;
		$_pass2 = isset($_POST['_pass2']) ? htmlspecialchars($_POST['_pass2'], ENT_QUOTES) : null ;
		$_pass3 = isset($_POST['_pass3']) ? htmlspecialchars($_POST['_pass3'], ENT_QUOTES) : null ;
			
		$cari = @sqlsrv_query($dbconnect, "SELECT * FROM WebMstUser WHERE KodeUser='".$id_aktif."' AND Password='".md5($_pass1)."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true)); $nums = @sqlsrv_num_rows($cari);
		if($nums === 0) {
			echo '<script type="text/javascript">sweetAlert("Maaf!", " Silahkan Masukkan Password Lama Yang Benar ", "error"); </script>';
		} else {
			if($_pass2 !== $_pass3){
				echo '<script type="text/javascript">sweetAlert("Maaf!", " Password Baru Yang Anda Masukkan Tidak Cocok ", "error"); </script>';
			} else {
				$update = @sqlsrv_query($dbconnect, "UPDATE WebMstUser SET Password='".md5($_pass3)."' WHERE KodeUser = '".$id_aktif."'") or die( print_r( sqlsrv_errors(), true));
				if($update){
					echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Update Data Tersimpan ", type: "success" },
					function () { window.location.href = "ubah-password.php"; });
						</script>';
				} else {
					echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Update Data Gagal ", type: "error" },
					function () { window.location.href = "ubah-password.php"; });
						</script>';
				}
			}
		}
	} ?>
	
    <div class="page">
      <!-- Main Navbar-->
	  <?php include "header.php"; ?>
	  <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include "sidebar.php"; ?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Data Akun</h2>
			</div>
          </header>
		  
		  <?php $query = @sqlsrv_query($dbconnect, "select * from WebMstUser where KodeUser = 'ADM-00001'") or die( print_r( sqlsrv_errors(), true));
		  while($data = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ $kode = $data['KodeUser']; $nama = $data['NamaUser']; $alamat = $data['Alamat']; $telp = $data['NoTelp']; $email = $data['Email']; $gambar = $data['Gambar']; } ?>
		  
          <!-- Dashboard Counts Section-->
          <section class="dashboard-counts no-padding-bottom">
            <div class="container-fluid">
              <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Ubah Password</h3>
                    </div>
                    <div class="card-body col-lg-6">
					  <form id="myform" method="post" action="">
						<div class="form-group">
							<input type="text" name="_pass1" placeholder="Password Lama" class="form-control" autocomplete="off">
						</div>
						<div class="form-group">
							<input type="text" name="_pass2" placeholder="Password Baru" class="form-control" autocomplete="off">
						</div>
						<div class="form-group">
							<input type="text" name="_pass3" placeholder="Konfirmasi Password Baru" class="form-control" autocomplete="off">
						</div>
						
						<button type="button" data-dismiss="modal" class="btn btn-secondary">Keluar</button>
						<button type="submit" name="_save_pass" class="btn btn-primary">Simpan</button>
					  </form>
						
					</div>  
                  </div>
              </div>
            </div>
          </section>
          
          <!-- Page Footer-->
          <?php include "footer.php"; ?>
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../component/vendor/jquery/jquery.min.js"></script>
    <script src="../component/vendor/popper.js/umd/popper.min.js"></script>
    <script src="../component/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../component/vendor/jquery.cookie/jquery.cookie.js"></script>
    <script src="../component/vendor/chart.js/Chart.min.js"></script>
    <script src="../component/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../component/js/charts-home.js"></script>
    <!-- Main File-->
    <script src="../component/js/front.js"></script>
	
	<!-- No Back Function -->
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
  </body>
</html>