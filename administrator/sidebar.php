<!-- Sidebar Section -->
<nav class="side-navbar">
	<!-- Sidebar Header-->
    <div class="sidebar-header d-flex align-items-center">
		<div class="avatar"><img src="../component/img/avatar-user.jpg" alt="Image" class="img-fluid rounded-circle"></div>
			<div class="title">
				<h1 class="h4">Admin</h1>
				<p>Administrator</p>
            </div>
        </div>
        <!-- Sidebar Navidation Menus -->
		<!-- <span class="heading">Main</span> -->
        <ul class="list-unstyled">
			<li class="active"><a href="default.php"><i class="fa fa-home"></i>Beranda</a></li>
            <li><a href="#KontenDropdown" aria-expanded="false" data-toggle="collapse"><i class="fa fa-edit"></i>Konten Website</a>
				<ul id="KontenDropdown" class="collapse list-unstyled ">
					<li><a href="mst-slider.php">Set Slider</a></li>
                    <li><a href="mst-post.php">Posting Artikel</a></li>
                    <li><a href="mst-galeri.php">Galeri Foto</a></li>
                    <li><a href="bukutamu.php">Testimoni</a></li>
                </ul>
            </li>
			<li><a href="#UserDropdown" aria-expanded="false" data-toggle="collapse"><i class="fa fa-users"></i>Manajemen User</a>
				<ul id="UserDropdown" class="collapse list-unstyled ">
					<li><a href="mst-user.php">Master User</a></li>
                    <li><a href="reset-pass.php">Reset Password</a></li>
                    <li><a href="server-log.php">Server Log</a></li>
                </ul>
            </li>
			<li><a href="#SettingDropdown" aria-expanded="false" data-toggle="collapse"><i class="fa fa-gear"></i>Setting</a>
				<ul id="SettingDropdown" class="collapse list-unstyled ">
					<li><a href="mst-setting.php">Setting Dasar</a></li>
                    <li><a href="mst-page.php">Setting Page</a></li>
                </ul>
            </li>
			<li><a href="#AkunDropdown" aria-expanded="false" data-toggle="collapse"><i class="fa fa-user"></i>Akun</a>
				<ul id="AkunDropdown" class="collapse list-unstyled ">
					<li><a href="ubah-profil.php">Lihat Profil</a></li>
                    <li><a href="ubah-password.php">Ubah Password</a></li>
                </ul>
            </li>
            <!-- <li><a href="#"><i class="fa fa-users"></i>Manajemen User</a></li>
            <li><a href="#"><i class="fa fa-wrench"></i>Setting</a></li>
            <li><a href="tables.html"><i class="icon-grid"></i>Tables</a></li>
            <li><a href="charts.html"><i class="fa fa-bar-chart"></i>Charts</a></li>
            <li><a href="forms.html"><i class="icon-padnote"></i>Forms</a></li>
            <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"><i class="icon-interface-windows"></i>Example dropdown</a>
				<ul id="exampledropdownDropdown" class="collapse list-unstyled ">
					<li><a href="#">Page</a></li>
                    <li><a href="#">Page</a></li>
                    <li><a href="#">Page</a></li>
                </ul>
            </li>
            <li><a href="login.html"><i class="icon-interface-windows"></i>Login page </a></li> -->
        </ul>
		<!-- <span class="heading">Extras</span>
		<ul class="list-unstyled">
			<li> <a href="#"> <i class="icon-flask"></i>Demo </a></li>
			<li> <a href="#"> <i class="icon-screen"></i>Demo </a></li>
			<li> <a href="#"> <i class="icon-mail"></i>Demo </a></li>
			<li> <a href="#"> <i class="icon-picture"></i>Demo </a></li>
		</ul> -->
</nav>