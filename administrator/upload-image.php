<?php 
require "../connections/config.php"; 
date_default_timezone_set('Asia/Jakarta'); 

// define GET parameter
$type = isset($_GET['type']) ? base64_decode($_GET['type']) : null ; 
$id = isset($_POST['_getid']) ? base64_decode($_POST['_getid']) : null ; 
$id2 = isset($_POST['_getidfoto']) ? base64_decode($_POST['_getidfoto']) : null ; 
$gbr = isset($_POST['_gbrid']) ? base64_decode($_POST['_gbrid']) : null ; 

// define random image code
function random($varchar){  
  $char = '1234567890abcdefghijkmnpqrstuvwxyz';  
  $string = '';  
  for($i = 0; $i < $varchar; $i++) {  
	$pos = rand(0, strlen($char)-1);  
	$string .= $char{$pos};  
  }  
return $string;  
}

if(isset($_POST)){
	// define process upload image 
	$ThumbSquareSize 		= 200; 			// thumbnail will be 200x200 pixels
	$BigImageMaxSize 		= 500; 			// image Maximum height or width
	$ThumbPrefix			= "thumb_"; 	// normal thumb Prefix
	$Quality 				= 100; 			// image quality
	if($type === '_image_slider'){
		$DestinationDirectory	= '../images/sliders/'; 	// specify upload directory ends with / (slash)
	} elseif($type === '_image_post'){
		$DestinationDirectory	= '../images/posts/'; 	// specify upload directory ends with / (slash)
	} elseif($type === '_image_galeri'){
		$DestinationDirectory	= '../images/galeries/'.$id.'/'; // specify upload directory ends with / (slash)
		if(!file_exists($DestinationDirectory)){
			@mkdir('../images/galeries/'.$id, 0777);
		}
	} elseif($type === '_image_userlogin'){
		$DestinationDirectory	= '../images/users/'; 	// specify upload directory ends with / (slash)
	} elseif($type === '_image_logo'){
		$DestinationDirectory	= '../images/assets/'; 	// specify upload directory ends with / (slash)
	} 
	
	// check if this is an ajax request
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
		die();
	}
	
	// check $_FILES['ImageFile'] not empty
	if(!isset($_FILES['ImageFile']) || !is_uploaded_file($_FILES['ImageFile']['tmp_name'])){
		die('Upload Bermasalah, Cek Ulang Ekstensi Image Yang Diupload!'); 	// output error when above checks fail.
	}
	
	// random number will be added after image name
	$RandomNumber 	= rand(0, 9999999999); 
	$ImageName 		= str_replace(' ','-',strtolower($_FILES['ImageFile']['name'])); 	// get image name
	$ImageSize 		= $_FILES['ImageFile']['size']; 									// get original image size
	$TempSrc	 	= $_FILES['ImageFile']['tmp_name']; 								// temp name of image file stored in PHP tmp folder
	$ImageType	 	= $_FILES['ImageFile']['type']; 									// get file type, returns "image/png", image/jpeg, text/plain etc.

	// let's check allowed $ImageType, we use PHP SWITCH statement here
	switch(strtolower($ImageType)){
		case 'image/png':
			// create a new image from file 
			$CreatedImage = imagecreatefrompng($_FILES['ImageFile']['tmp_name']);
			break;
		case 'image/gif':
			$CreatedImage = imagecreatefromgif($_FILES['ImageFile']['tmp_name']);
			break;			
		case 'image/jpeg':
		case 'image/pjpeg':
			$CreatedImage = imagecreatefromjpeg($_FILES['ImageFile']['tmp_name']);
			break;
		default:
			die('File Tidak Support!'); 	// output error and exit
	}
	
	// PHP getimagesize() function returns height/width from image file stored in PHP tmp folder.
	// get first two values from image, width and height. 
	// list assign svalues to $CurWidth,$CurHeight
	list($CurWidth,$CurHeight) = getimagesize($TempSrc);
	
	// get file extension from Image name, this will be added after random name
	$ImageExt = substr($ImageName, strrpos($ImageName, '.'));
  	$ImageExt = str_replace('.','',$ImageExt);
	
	// remove extension from filename
	$ImageName 		= preg_replace("/\\.[^.\\s]{3,4}$/", "", $ImageName); 
	
	// construct a new name with random number and extension.
	$NewImageName = str_replace('-','',$id).'_'.random(10).'.'.$ImageExt;
	
	// set the Destination Image
	$thumb_DestRandImageName 	= $DestinationDirectory.$ThumbPrefix.$NewImageName; 	// thumbnail name with destination directory
	$DestRandImageName 			= $DestinationDirectory.$NewImageName; 					// image with destination directory
	
	// resize image to Specified Size by calling resizeImage function.
	if(resizeImage($CurWidth,$CurHeight,$BigImageMaxSize,$DestRandImageName,$CreatedImage,$Quality,$ImageType)){
		// create a square Thumbnail right after, this time we are using cropImage() function
		if(!cropImage($CurWidth,$CurHeight,$ThumbSquareSize,$thumb_DestRandImageName,$CreatedImage,$Quality,$ImageType)){
			echo 'Create Thumbnail Gagal!';
		}
		
		// we have succesfully resized and created thumbnail image
		// we can now output image to user's browser or store information in the database
		
		if($type === '_image_slider'){
			echo '<img src="../images/sliders/'.$ThumbPrefix.$NewImageName.'" alt="Thumbnail" title="Gambar Thumbnail" class="img img-thumbnail">';
		} elseif($type === '_image_post'){
			echo '<img src="../images/posts/'.$ThumbPrefix.$NewImageName.'" alt="Thumbnail" title="Gambar Thumbnail" class="img img-thumbnail">';
		} elseif($type === '_image_galeri'){
			echo '<img src="../images/galeries/'.$id.'/'.$ThumbPrefix.$NewImageName.'" alt="Thumbnail" title="Gambar Thumbnail" class="img img-thumbnail">';
		} elseif($type === '_image_userlogin'){
			echo '<img src="../images/users/'.$ThumbPrefix.$NewImageName.'" alt="Thumbnail" title="Gambar Thumbnail" class="img img-thumbnail">';
		} elseif($type === '_image_logo'){
			echo '<img src="../images/assets/'.$ThumbPrefix.$NewImageName.'" alt="Thumbnail" title="Gambar Thumbnail" class="img img-thumbnail">';
		} 
		
		// Insert info into database table!
		if($type === '_image_slider'){
			$path = "../images/sliders/".$gbr; $path_thumb = "../images/sliders/thumb_".$gbr;
			if($gbr == null){
				$query = @sqlsrv_query($dbconnect, "UPDATE WebMstSlider SET Gambar = '$NewImageName' where KodeSlider = '$id'");
			} else {
				if(file_exists(@$path)){ @unlink($path); }
				if(file_exists(@$path_thumb)){ @unlink($path_thumb); }
				$query = @sqlsrv_query($dbconnect, "UPDATE WebMstSlider SET Gambar = '$NewImageName' where KodeSlider = '$id'");
			}
			if($query){
				echo '<script type="text/javascript">
				sweetAlert({ title: "Berhasil!", text: " Upload Gambar Sukses ", type: "success" },
				function () { window.location.href = "mst-slider.php"; });
				</script>';
			} else {
				echo '<script type="text/javascript">sweetAlert("Maaf!", " Upload Gambar Gagal ", "error"); </script>';
			}
		} elseif($type === '_image_post'){
			$path = "../images/posts/".$gbr; $path_thumb = "../images/posts/thumb_".$gbr;
			if($gbr == null){
				$query = @sqlsrv_query($dbconnect, "UPDATE WebMstPost SET Gambar = '$NewImageName' where KodePost = '$id'");
			} else {
				if(file_exists(@$path)){ @unlink($path); }
				if(file_exists(@$path_thumb)){ @unlink($path_thumb); }
				$query = @sqlsrv_query($dbconnect, "UPDATE WebMstPost SET Gambar = '$NewImageName' where KodePost = '$id'");
			}
			if($query){
				echo '<script type="text/javascript">
				sweetAlert({ title: "Berhasil!", text: " Upload Gambar Sukses ", type: "success" },
				function () { window.location.href = "mst-post.php"; });
				</script>';
			} else {
				echo '<script type="text/javascript">sweetAlert("Maaf!", " Upload Gambar Gagal ", "error"); </script>';
			}
		} elseif($type === '_image_galeri'){
			$path = "../images/galeries/".$id."/".$gbr; $path_thumb = "../images/galeries/".$id."/thumb_".$gbr;
			// membuat kode baru
			$sql = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodeFoto,1)) AS kode FROM WebFoto WHERE KodeGaleri='$id'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
			$nums = @sqlsrv_num_rows($sql); 
			while($data = @sqlsrv_fetch_array($sql)){
				if($nums === 0){ $kode = 1; } else { $kode = $data['kode'] + 1; }
			}
			$code = $kode;
			
			if($gbr == null){
				$query = @sqlsrv_query($dbconnect, "INSERT into WebFoto (KodeFoto,Gambar,LastUpdated,KodeGaleri) values ('$code','".$NewImageName."','".date('Y-m-d H:i:s')."','$id')");
			} else {
				if(file_exists(@$path)){ @unlink($path); }
				if(file_exists(@$path_thumb)){ @unlink($path_thumb); }
				$query = @sqlsrv_query($dbconnect, "UPDATE WebFoto SET Gambar = '".$NewImageName."' where KodeFoto = '$id2' AND KodeGaleri = '$id'");
			}
			if($query){
				echo '<script type="text/javascript">
				sweetAlert({ title: "Berhasil!", text: " Upload Gambar Sukses ", type: "success" },
				function () { window.location.href = "mst-galeri.php?page='.htmlspecialchars(base64_encode("_view_galeri")).'&id='.htmlspecialchars(base64_encode($id)).'"; });
				</script>';
			} else {
				echo '<script type="text/javascript">sweetAlert("Maaf!", " Upload Gambar Gagal ", "error"); </script>';
			}
		} elseif($type === '_image_userlogin'){
			$path = "../images/users/".$gbr; $path_thumb = "../images/users/thumb_".$gbr;
			if($gbr == null){
				$query = @sqlsrv_query($dbconnect, "UPDATE WebMstUser SET Gambar = '$NewImageName' where KodeUser = '$id'");
			} else {
				if(file_exists($path)){ @unlink($path); }
				if(file_exists($path_thumb)){ @unlink($path_thumb); }
				$query = @sqlsrv_query($dbconnect, "UPDATE WebMstUser SET Gambar = '$NewImageName' where KodeUser = '$id'");
			}
			if($query){
				echo '<script type="text/javascript">
				sweetAlert({ title: "Berhasil!", text: " Upload Gambar Sukses ", type: "success" },
				function () { window.location.href = "ubah-profil.php"; });
				</script>';
			} else {
				echo '<script type="text/javascript">sweetAlert("Maaf!", " Upload Gambar Gagal ", "error"); </script>';
			}
		} elseif($type === '_image_logo'){
			$path = "../images/assets/".$gbr; $path_thumb = "../images/assets/thumb_".$gbr;
			if($gbr == null){
				$query = @sqlsrv_query($dbconnect, "UPDATE WebSettingDasar SET Value = '$NewImageName' where NoUrut = '$id'");
			} else {
				if(file_exists($path)){ @unlink($path); }
				if(file_exists($path_thumb)){ @unlink($path_thumb); }
				$query = @sqlsrv_query($dbconnect, "UPDATE WebSettingDasar SET Value = '$NewImageName' where NoUrut = '$id'");
			}
			if($query){
				echo '<script type="text/javascript">
				sweetAlert({ title: "Berhasil!", text: " Upload Gambar Sukses ", type: "success" },
				function () { window.location.href = "mst-setting.php"; });
				</script>';
			} else {
				echo '<script type="text/javascript">sweetAlert("Maaf!", " Upload Gambar Gagal ", "error"); </script>';
			}
		} else {
			die('Resize Gagal!'); //output error
		}
	}
}

// this function will proportionally resize image 
function resizeImage($CurWidth,$CurHeight,$MaxSize,$DestFolder,$SrcImage,$Quality,$ImageType){
	// check Image size is not 0
	if($CurWidth <= 0 || $CurHeight <= 0){
		return false;
	}
	
	// construct a proportional size of new image
	$ImageScale = min($MaxSize/$CurWidth, $MaxSize/$CurHeight); 
	$NewWidth  	= ceil($ImageScale*$CurWidth);
	$NewHeight 	= ceil($ImageScale*$CurHeight);
	$NewCanves 	= imagecreatetruecolor($NewWidth, $NewHeight);
	
	// resize Image
	if(imagecopyresampled($NewCanves, $SrcImage,0, 0, 0, 0, $NewWidth, $NewHeight, $CurWidth, $CurHeight)){
		switch(strtolower($ImageType)){
			case 'image/png':
				imagepng($NewCanves,$DestFolder);
				break;
			case 'image/gif':
				imagegif($NewCanves,$DestFolder);
				break;			
			case 'image/jpeg':
			case 'image/pjpeg':
				imagejpeg($NewCanves,$DestFolder,$Quality);
				break;
			default:
				return false;
	}
	
	// destroy image, frees memory	
	if(is_resource($NewCanves)) { imagedestroy($NewCanves); } 
		return true;
	}

}

// this function corps image to create exact square images, no matter what its original size!
function cropImage($CurWidth,$CurHeight,$iSize,$DestFolder,$SrcImage,$Quality,$ImageType){	 
	// check Image size is not 0
	if($CurWidth <= 0 || $CurHeight <= 0){
		return false;
	}
	
	// abeautifulsite.net has excellent article about "Cropping an Image to Make Square bit.ly/1gTwXW9
	if($CurWidth>$CurHeight){
		$y_offset = 0;
		$x_offset = ($CurWidth - $CurHeight) / 2;
		$square_size = $CurWidth - ($x_offset * 2);
	}else{
		$x_offset = 0;
		$y_offset = ($CurHeight - $CurWidth) / 2;
		$square_size = $CurHeight - ($y_offset * 2);
	}
	
	$NewCanves = imagecreatetruecolor($iSize, $iSize);	
	if(imagecopyresampled($NewCanves, $SrcImage,0, 0, $x_offset, $y_offset, $iSize, $iSize, $square_size, $square_size)){
		switch(strtolower($ImageType)){
			case 'image/png':
				imagepng($NewCanves,$DestFolder);
				break;
			case 'image/gif':
				imagegif($NewCanves,$DestFolder);
				break;			
			case 'image/jpeg':
			case 'image/pjpeg':
				imagejpeg($NewCanves,$DestFolder,$Quality);
				break;
			default:
				return false;
		}
	// destroy image, frees memory	
	if(is_resource($NewCanves)) { imagedestroy($NewCanves); } 
		return true;
	}
	  
}

?>		