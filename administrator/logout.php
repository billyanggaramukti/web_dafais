<?php
session_start();
unset($_SESSION['_userlogin']); 
session_destroy(); 
?>

<!DOCTYPE html>
<html>
	<head>
		<?php include "head.php"; ?>
		<!-- Sweet Alerts -->
		<link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
		<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	</head>
	<body>
	<?php $id = isset($_GET['id']) ? base64_decode($_GET['id']) : 0 ;
	if($id === "error") { ?>
	<script type="text/javascript">
		sweetAlert({
		title: "Maaf!", text: " Anda Harus Sign In ", type: "error" },
		function () { window.location.href = "../index.php"; });
	</script>
	<?php } else { ?>
	<script type="text/javascript">
		sweetAlert({ title: "Terima Kasih!", text: " Anda Telah Sign Out ", type: "info" },
		function () { window.location.href = "../index.php"; });
	</script>
	<?php } ?>
	</body>
</html>




