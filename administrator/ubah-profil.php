<?php require "../connections/config.php"; include "akses.php";

// get variable POST
$page = isset($_GET['page']) ? @$_GET['page'] : null ; 

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include "head.php"; ?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../component/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../component/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../component/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../component/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../component/css/custom.css">
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	<!-- Upload Image -->
	<link rel="stylesheet" href="../library/upload-image/upload-img.css" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <?php if($page === "_save_userlogin"){
		// mengambil POST update user
		$_nama = isset($_POST['_nama']) ? htmlspecialchars($_POST['_nama'], ENT_QUOTES) : null ;
		$_alamat = isset($_POST['_alamat']) ? htmlspecialchars($_POST['_alamat'], ENT_QUOTES) : null ;
		$_telp = isset($_POST['_telp']) ? htmlspecialchars($_POST['_telp'], ENT_QUOTES) : null ;
		$_email = isset($_POST['_email']) ? htmlspecialchars($_POST['_email'], ENT_QUOTES) : null ;
		$_user = isset($_POST['_user']) ? htmlspecialchars($_POST['_user'], ENT_QUOTES) : null ;
		$_pass = isset($_POST['_password']) ? htmlspecialchars($_POST['_password'], ENT_QUOTES) : null ;
			
		if($id_aktif != null){
			// update data user
			$cari2 = @sqlsrv_query($dbconnect, "SELECT * FROM WebMstUser WHERE Email='".$_email."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true)); $nums2 = @sqlsrv_num_rows($cari2);
			if($nums2 <> 0) {
				echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Email Sudah Ada ", type: "error" },
				function () { window.location.href = "ubah-profil.php"; });
					</script>';
			} else {
				$update = @sqlsrv_query($dbconnect, "UPDATE WebMstUser SET NamaUser='$_nama', Alamat='$_alamat', NoTelp='$_telp', Email='$_email', LastUpdated='".date('Y-m-d H:i:s')."' WHERE KodeUser = '".$id_aktif."'") or die( print_r( sqlsrv_errors(), true));
				if($update){
					echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Update Data Tersimpan ", type: "success" },
					function () { window.location.href = "ubah-profil.php"; });
						</script>';
				} else {
					echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Update Data Gagal ", type: "error" },
					function () { window.location.href = "ubah-profil.php"; });
						</script>';
				}
			}
		}
			
	} ?>
	
    <div class="page">
      <!-- Main Navbar-->
	  <?php include "header.php"; ?>
	  <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include "sidebar.php"; ?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Data Akun</h2>
			</div>
          </header>
		  
		  <!-- Modal Update Data -->
          <div id="updateData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
            <div role="document" class="modal-dialog">
              <div class="modal-content">
                  <div class="fetched-data"></div>
              </div>
            </div>
          </div>
		  
		  <!-- Modal Upload Image -->
          <div class="modal fade" id="uploadImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
			  <div class="modal-content">
				<div class="modal-header">
					<h4 id="exampleModalLabel" class="modal-title">Upload Gambar</h4>
					<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span class="fa fa-close"aria-hidden="true"></span></button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<span>Tipe file disarankan *.png, *.gif atau *.jpg max 2 MB</span><br><br>
						<div class="row"><div class="col-lg-4"><div id="_output"></div></div></div>
						<form action="upload-image.php?type=<?php echo base64_encode('_image_userlogin'); ?>" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
							<input class="form-control" type="hidden" name="_getid" readonly>
							<input class="form-control" type="hidden" name="_gbrid" readonly>
							<input name="ImageFile" id="_imageInput" type="file"/><br><br>
							<input type="submit" id="_submit-btn" name="_submit-btn" class="btn btn-default" value="Upload" />
						</form>
						<div class="row">
						<div class="col-lg-12">
							<img src="../images/assets/ajax-loader.gif" id="_loading-img" style="display:none;" alt="Please Wait"/><br><br>
							<div id="_progressbox" style="display:none;"><div id="_progressbar"></div><div id="_statustxt">0%</div></div>
						</div>
					</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Keluar</button>
				</div>
			  </div>
              <!-- /.modal-content -->
			</div>
            <!-- /.modal-dialog -->
		  </div>
          <!-- /.modal -->
		  
		  <?php $query = @sqlsrv_query($dbconnect, "select * from WebMstUser where KodeUser = '".$id_aktif."'") or die( print_r( sqlsrv_errors(), true));
		  while($data = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)){ $kode = $data['KodeUser']; $nama = $data['NamaUser']; $alamat = $data['Alamat']; $telp = $data['NoTelp']; $email = $data['Email']; $gambar = $data['Gambar']; } ?>
		  
          <!-- Dashboard Counts Section-->
          <section class="dashboard-counts no-padding-bottom">
            <div class="container-fluid">
              <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Profil User &nbsp;
						<button href="#updateData" class="btn btn-sm btn-success" data-toggle="modal" data-id="<?php echo htmlspecialchars(base64_encode($kode)); ?>">Ubah Profil</button>
						<?php if($gambar == null OR $gambar === '') {
							echo '<button href="#uploadImage" class="btn btn-sm btn-primary" data-toggle="modal" data-id="'.htmlspecialchars(base64_encode($kode)).'">Ubah Foto</button>';
						} else {
							echo '<button href="#uploadImage" class="btn btn-sm btn-primary" data-toggle="modal" data-id="'.htmlspecialchars(base64_encode($kode)).'" data-gbr="'.htmlspecialchars(base64_encode($gambar)).'">Ubah Foto</button>';
						} ?>
					  </h3>
                    </div>
                    <div class="card-body">
						<div class="row">
						<div class="col-lg-3">
							<div class="form-group">
								<dl>
									<?php if($gambar == null OR $gambar === '') {
										echo '<dd><div class="avatar"><img src="../component/img/avatar-user.jpg" alt="Image" class="img-fluid rounded-circle"></div><dd><br>';
									} else {
										echo '<dd><div class="avatar"><img src="../images/users/thumb_'.$gambar.'" alt="Image" class="img-fluid rounded-circle"></div><dd><br>';
									} ?>
								</dl>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<dl>
									<dt>Nama User</dt><dd><?php echo ucwords($nama); ?></dd><hr>
									<dt>Informasi</dt><dd><?php echo "Alamat : <br>".ucwords($alamat)."<br>Kontak Person : <br>".$telp."<br>Email : <br>".$email; ?></dd><hr>
								</dl>
							</div>
						</div>
						</div>
					</div>  
                  </div>
              </div>
            </div>
          </section>
          
          <!-- Page Footer-->
          <?php include "footer.php"; ?>
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../component/vendor/jquery/jquery.min.js"></script>
    <script src="../component/vendor/popper.js/umd/popper.min.js"></script>
    <script src="../component/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../component/vendor/jquery.cookie/jquery.cookie.js"></script>
    <script src="../component/vendor/chart.js/Chart.min.js"></script>
    <script src="../component/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../component/js/charts-home.js"></script>
    <!-- Main File-->
    <script src="../component/js/front.js"></script>
	
	<!-- No Back Function -->
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<!-- Link to Modal Add and Edit Data -->
	<script type="text/javascript">
    $(document).ready(function(){
        $('#updateData').on('show.bs.modal', function(e) {
			var rowid = $(e.relatedTarget).data('id');
            $.ajax({
				type : 'GET', url : 'update-data.php?page=<?php echo htmlspecialchars(base64_encode("_update_userlogin")); ?>', data : 'rowid='+ rowid,
				success : function(data){
					$('.fetched-data').html(data);
				}
            });
         });
    });
	</script>
	
	<!-- Parameter Upload Image -->
	<script type="text/javascript">
	$(document).ready(function(){
		$('#uploadImage').on('show.bs.modal', function(e) {
			var getid = $(e.relatedTarget).data('id');
			var gbrid = $(e.relatedTarget).data('gbr');
			var tmbid = $(e.relatedTarget).data('tmb');
			$(e.currentTarget).find('input[name="_getid"]').val(getid);
			$(e.currentTarget).find('input[name="_gbrid"]').val(gbrid);
			$(e.currentTarget).find('input[name="_tmbid"]').val(tmbid);
		});
	});
	</script>
	
	<!-- Progres Bar Upload Image -->
	<script type="text/javascript" src="../library/upload-image/jquery.form.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() { 
			var progressbox     = $('#_progressbox');
			var progressbar     = $('#_progressbar');
			var statustxt       = $('#_statustxt');
			var completed       = '0%';
			var options = { 
					target: '#_output',   			// target element(s) to be updated with server response 
					beforeSubmit: beforeSubmit,  	// pre-submit callback 
					uploadProgress: OnProgress,
					success: afterSuccess,  		// post-submit callback 
					resetForm: true        			// reset the form after successful submit 
				}; 
				
			 $('#MyUploadForm').submit(function() { 
					$(this).ajaxSubmit(options);  	// return false to prevent standard browser submit and page navigation 
					return false; 
				});
			
		// when upload progresses	
		function OnProgress(event, position, total, percentComplete) {
			// progress bar
			progressbar.width(percentComplete + '%') 	// update progressbar percent complete
			statustxt.html(percentComplete + '%'); 		// update status text
			if(percentComplete>50) {
				statustxt.css('color','#fff'); 		// change status text to white after 50%
			}
		}

		// after succesful upload
		function afterSuccess() {
			$('#_imageInput').hide(); 	// hide input image
			$('#_submit-btn').hide(); 	// hide submit button
			$('#_loading-img').hide(); 	// hide loading 
			$('#_statustxt').hide(); 	// hide status bar 
			$('#_progressbox').hide();  // hide progress
			$('#_oldimg').hide(); 		// hide old image
		}

		// function to check file size before uploading.
		function beforeSubmit() {
			// check whether browser fully supports all File API
			if(window.File && window.FileReader && window.FileList && window.Blob) {
				// check empty input filed
				if(!$('#_imageInput').val()) {	
					sweetAlert("Maaf!", " Masukkan Gambar Terlebih Dahulu ", "error");
					// $("#_output").html("Masukkan gambar terlebih dahulu!");
					return false
				}
				
				var fsize = $('#_imageInput')[0].files[0].size;		// get file size
				var ftype = $('#_imageInput')[0].files[0].type; 	// get file type
				
				// allow only valid image file types 
				switch(ftype){
					case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
						break;
					default:
						sweetAlert("Maaf!", " Tipe File Harus .PNG, .GIF, atau .JPG ", "error");
						// $("#_output").html("<b>"+ftype+"</b> Tipe File Anda Tidak Support!");
						return false
				}
				
				// allowed file size is less than 2 MB (2048576)
				if(fsize>2048576) {
					sweetAlert("Maaf!", " Ukuran File Terlalu Besar ", "error");
					// $("#_output").html("<b>"+bytesToSize(fsize) +"</b> File image terlalu besar! <br/>Perkecil ukuran file terlebih dahulu");
					return false
				}
				
				// progress bar
				progressbox.show(); 			// show progressbar
				progressbar.width(completed); 	// initial value 0% of progressbar
				statustxt.html(completed); 		// set status text
				statustxt.css('color','#000'); 	// initial color of status text
				
				$('#_submit-btn').hide(); 		// hide submit button
				$('#_loading-img').show(); 		// show loading image
				$("#_output").html("");  
				
			}
			else
			{
				// output error to older unsupported browsers that doesn't support HTML5 File API
				sweetAlert("Maaf!", " Browser Anda Tidak Support Fitur Ini ", "error");
				// $("#_output").html("Upgrade browser anda! Browser tidak mendukung fitur ini");
				return false;
			}
		}

		// function to format bites bit.ly/19yoIPO
		function bytesToSize(bytes) {
		   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
		   if (bytes == 0) return '0 Bytes';
		   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
		   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
		}

		}); 

	</script>
	
  </body>
</html>