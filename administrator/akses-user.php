<?php 
require "../connections/config.php"; 
date_default_timezone_set('Asia/Jakarta'); 

// get variable POST
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : null ; 
$id = isset($_GET['rowid']) ? base64_decode($_GET['rowid']) : null ; 

?>

<div class="modal-header">
	<h4 id="exampleModalLabel" class="modal-title">Set Akses</h4>
	<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span class="fa fa-close"aria-hidden="true"></span></button>
</div>

<!-- Modal Slider -->
<?php if($page === "_akses_user") { 
	echo '<form id="myform" method="post" action="mst-user.php?page=_save_akses&id='.htmlspecialchars(base64_encode($id)).'">';
?>

<div class="modal-body">
	<div class="table-responsive">                       
        <table class="table table-striped table-hover">
            <thead>
                <tr>
					<th>No</th>
                    <th>Menu</th>
                    <th>Aksi</th>
                </tr>
            </thead>
			<tbody>
			<?php $no = 0; $query = @sqlsrv_query($dbconnect, "select * from WebMenu ORDER BY KodeMenu") or die( print_r( sqlsrv_errors(), true));
			while($data = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)) { 
				echo '<tr class="text-left">';
				echo '<td width="5%">'.++$no.'</th>';
				echo '<td>'.$data['NamaMenu'].'</td>';
				echo '<td width="15%" align="center">';
					$cari = @sqlsrv_query($dbconnect, "select * from WebServerFitur WHERE KodeUser='".$id."' AND KodeMenu='".$data['KodeMenu']."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
					$nums = @sqlsrv_fetch_array($cari);
					if($nums <> 0){
						echo '<a href="mst-user.php?page=_delete_akses&id='.htmlspecialchars(base64_encode($id)).'&akses='.htmlspecialchars(base64_encode($data["KodeMenu"])).'" class="confirm-delete">
						<span class="btn btn-sm btn-success" title="Hapus Data">Verified</span></a>';
					} else {
						echo '<input type="checkbox" id="cekbox" name="cekbox[]" value="'.$data["KodeMenu"].'"/>';
					}
				echo '</td>';
			}
			$no++;
			?>
			</tbody>
			<input type="button" class="btn btn-sm btn-warning" onclick="cek(this.form.cekbox)" value="Select All" />&nbsp;
			<input type="button" class="btn btn-sm btn-danger" onclick="uncek(this.form.cekbox)" value="Clear All" /><br><br>
		</table>
	</div>
</div>

<?php } ?>

<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-secondary">Keluar</button>
	<button type="submit" class="btn btn-primary">Simpan</button>
    </form>
</div>

<!-- ZebraDatepicker JavaScript -->
<link href="../library/zebra-datepicker/css/default.css" rel="stylesheet" type="text/css">
<script src="../library/zebra-datepicker/javascript/zebra_datepicker.js"></script>
<script src="../library/zebra-datepicker/javascript/zebra_datepicker.src.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	$('#tanggal1').Zebra_DatePicker();
});
</script>

<!-- fungsi untuk validasi form input atau edit -->
<script type="text/javascript">
function validasi_person() {
	var number = /^[0-9]+$/;
	var username = /^[a-zA-Z0-9\_\-]{6,100}$/;
	var mail = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	
	// validasi person
	var nomor = document.forms["myform"]["nomor"].value;
	var email = document.forms["myform"]["email"].value;
		if (!nomor.match(number)) {
			sweetAlert("Maaf!", " Kontak Harus Diisi Angka ", "error");
			return false;
		};
		if (!email.match(mail)) {
			sweetAlert("Maaf!", " Format Email Anda Salah ", "error");
			return false;
		};
}
</script>

<!-- fungsi untuk checkbox data -->
<script>
function cek(cekbox){
	for(i=0; i < cekbox.length; i++){
		cekbox[i].checked = true;
	}
}
function uncek(cekbox){
	for(i=0; i < cekbox.length; i++){
		cekbox[i].checked = false;
	}
}
</script>