<?php require "../connections/config.php"; require "../library/tgl-indo.php"; include "akses.php";

// get variable POST
$id = isset($_GET['id']) ? htmlspecialchars(base64_decode(@$_GET['id'])) : null ; 
$page = isset($_GET['page']) ? @$_GET['page'] : null ; 
$keyword = isset($_REQUEST['_keyword']) ? @$_REQUEST['_keyword'] : null ; 
$aktif = isset($_REQUEST['_aktif']) ? @$_REQUEST['_aktif'] : null ; 

// hak akses
$nopage = 2; require "../library/lock-menu.php";
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php include "head.php"; ?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../component/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../component/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="../component/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../component/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../component/css/custom.css">
	<!-- Sweet Alerts -->
	<link rel="stylesheet" href="../library/sweetalert/sweetalert.css" rel="stylesheet">
	<script src="../library/sweetalert/sweetalert.min.js" type="text/javascript"></script>
	<!-- Upload Image -->
	<link rel="stylesheet" href="../library/upload-image/upload-img.css" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <?php if($page === "_save_post"){
		// membuat id data otomatis
		$sql = @sqlsrv_query($dbconnect, "SELECT MAX(RIGHT(KodePost,5)) AS kode FROM WebMstPost WHERE LEFT(KodePost,10)='PST-".date('Ym')."'", array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
		$nums = @sqlsrv_num_rows($sql); 
		while($data = @sqlsrv_fetch_array($sql)){
			if($nums === 0){ $kode = 1; } else { $kode = $data['kode'] + 1; }
		}
		// membuat kode baru
		$code = str_pad($kode, 5, "0", STR_PAD_LEFT);
		$_create_code = "PST-".date('Ym')."-".$code;
				
		// mengambil POST add or update post
		$_title = isset($_POST['_title']) ? htmlspecialchars($_POST['_title'], ENT_QUOTES) : null ;
		$_descript = isset($_POST['_descript']) ? $_POST['_descript'] : null ;
		$_tgl = isset($_POST['_tanggal']) ? $_POST['_tanggal'] : null ;
		$_author = isset($_POST['_author']) ? htmlspecialchars($_POST['_author'], ENT_QUOTES) : null ;
			
		if($id == null){
			// insert data post
			$insert = @sqlsrv_query($dbconnect, "INSERT into WebMstPost (KodePost,Tanggal,Judul,Deskripsi,Gambar,Author,IsAktif,LastUpdated) values ('$_create_code','$_tgl','$_title','$_descript',null,'$_author','1','".date('Y-m-d H:i:s')."')") or die( print_r( sqlsrv_errors(), true));
			if($insert){
				echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Input Data Tersimpan ", type: "success" },
				function () { window.location.href = "mst-post.php"; });
				</script>';
			} else {
				echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Input Data Gagal ", type: "error" },
				function () { window.location.href = "mst-post.php"; });
				</script>';
			}
		} else {
			// update data post
			$update = @sqlsrv_query($dbconnect, "UPDATE WebMstPost SET Tanggal='$_tgl', Judul='$_title', Deskripsi='$_descript', Author='$_author', LastUpdated='".date('Y-m-d H:i:s')."' WHERE KodePost = '".$id."'") or die( print_r( sqlsrv_errors(), true));
			if($update){
				echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Update Data Tersimpan ", type: "success" },
				function () { window.location.href = "mst-post.php"; });
				</script>';
			} else {
				echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Update Data Gagal ", type: "error" },
				function () { window.location.href = "mst-post.php"; });
				</script>';
			}
		}
			
	} elseif($id !== null AND $page === "_nonaktif"){
		// non aktif slider
		$query = @sqlsrv_query($dbconnect, "UPDATE WebMstPost SET IsAktif = '0' WHERE KodePost = '".$id."'") or die( print_r( sqlsrv_errors(), true));
		echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Non Aktif Data Berhasil ", type: "success" },
		function () { window.location.href = "mst-post.php"; }); </script>';
		
	} elseif($id !== null AND $page === "_aktif"){
		// aktif slider
		$query = @sqlsrv_query($dbconnect, "UPDATE WebMstPost SET IsAktif = '1' WHERE KodePost = '".$id."'") or die( print_r( sqlsrv_errors(), true));
		echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Aktif Data Berhasil ", type: "success" },
		function () { window.location.href = "mst-post.php"; }); </script>';
		
	} elseif($id !== null AND $page === "_delete_slider"){
		// delete slider
		$cek = @sqlsrv_query($dbconnect, "SELECT IsAktif, Gambar from WebMstPost WHERE KodePost = '".$id."'") or die( print_r( sqlsrv_errors(), true));
		$cari = @sqlsrv_fetch_array($cek, SQLSRV_FETCH_ASSOC);
		if($cari['IsAktif'] === 1){
			echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Non Aktifkan Post Terlebih Dahulu ", type: "error" },
			function () { window.location.href = "mst-post.php"; }); </script>';
		} else {
			// delete upload image
			$file = "../images/posts/".$cari['Gambar']; $file2 = "../images/posts/thumb_".$cari['Gambar']; 
			if($cari['Gambar'] != null OR $cari['Gambar'] !== "") {
				if(file_exists(@$file)){ @unlink($file); }
				if(file_exists(@$file2)){ @unlink($file2); }
			}	
			$delete = @sqlsrv_query($dbconnect, "DELETE from WebMstPost WHERE KodePost = '".$id."'") or die( print_r( sqlsrv_errors(), true));
			if($delete){
				echo '<script type="text/javascript">sweetAlert({ title: "Berhasil!", text: " Delete Data Sukses ", type: "success" },
				function () { window.location.href = "mst-post.php"; }); </script>';
			} else {
				echo '<script type="text/javascript">sweetAlert({ title: "Maaf!", text: " Delete Data Gagal ", type: "error" },
				function () { window.location.href = "mst-post.php"; }); </script>';
			}
		}
		
	} ?>
	
    <div class="page">
      <!-- Main Navbar-->
	  <?php include "header.php"; ?>
	  <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php include "sidebar.php"; ?>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Posting Artikel &nbsp;
			  <button href="#updateData" class="btn btn-sm btn-primary" data-toggle="modal" data-id="<?php echo htmlspecialchars(base64_encode(null)); ?>"><i class="fa fa-plus"></i> &nbsp;Tambah</button>
			  </h2>
			</div>
          </header>
		  
		  <!-- Modal Update Data -->
          <div id="updateData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
            <div role="document" class="modal-dialog">
              <div class="modal-content">
                  <div class="fetched-data"></div>
              </div>
            </div>
          </div>
		  
		  <!-- Modal Detail Data -->
          <div id="detailData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
            <div role="document" class="modal-dialog">
              <div class="modal-content">
                  <div class="fetched-data"></div>
              </div>
            </div>
          </div>
		  
		  <!-- Modal Upload Image -->
          <div class="modal fade" id="uploadImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
			  <div class="modal-content">
				<div class="modal-header">
					<h4 id="exampleModalLabel" class="modal-title">Upload Gambar</h4>
					<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span class="fa fa-close"aria-hidden="true"></span></button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<span>Tipe file disarankan *.png, *.gif atau *.jpg max 2 MB</span><br><br>
						<div class="row"><div class="col-lg-4"><div id="_output"></div></div></div>
						<form action="upload-image.php?type=<?php echo base64_encode('_image_post'); ?>" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
							<input class="form-control" type="hidden" name="_getid" readonly>
							<input class="form-control" type="hidden" name="_gbrid" readonly>
							<input name="ImageFile" id="_imageInput" type="file"/><br><br>
							<input type="submit" id="_submit-btn" name="_submit-btn" class="btn btn-default" value="Upload" />
						</form>
						<div class="row">
						<div class="col-lg-12">
							<img src="../images/assets/ajax-loader.gif" id="_loading-img" style="display:none;" alt="Please Wait"/><br><br>
							<div id="_progressbox" style="display:none;"><div id="_progressbar"></div><div id="_statustxt">0%</div></div>
						</div>
					</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Keluar</button>
				</div>
			  </div>
              <!-- /.modal-content -->
			</div>
            <!-- /.modal-dialog -->
		  </div>
          <!-- /.modal -->
		  
          <!-- Dashboard Counts Section-->
          <section class="dashboard-counts no-padding-bottom">
            <div class="container-fluid">
              <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">List Data</h3>
                    </div>
                    <div class="card-body">
					  <form method="post" action="">
						 <div class="form-group input-group">
						    <div class="col-lg-6">
							   <?php if(@$_REQUEST['aktif'] == null){
									echo '<input type="checkbox" id="option" name="aktif" value="1" />';
								} else {
									echo '<input type="checkbox" id="option" name="aktif" value="1" checked="checked" />';
								} 
							   ?>
							   <label for="option">&nbsp;Tampilkan Data Non Aktif</label>
							</div>
							<div class="col-lg-6">
							   <div class="input-group">
								  <input type="text" name="keyword" class="form-control" placeholder="Judul Posting" value="<?php echo htmlspecialchars(@$_REQUEST['keyword']); ?>" autocomplete="off">
								  <div class="input-group-append"><button type="submit" class="btn btn-primary">Cari</button></div>
							   </div>
							</div>
						 </div>
					  </form>
						
					  <div class="table-responsive">                       
                        <table class="table table-striped table-hover">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Gambar</th>
                              <th>Deskripsi</th>
                              <th>Status</th>
                              <th>Aksi</th>
                            </tr>
                          </thead>
						  
						  <?php 
						  //procedure paging
						  require "../connections/config.php";
						  include "../library/pagination.php";
						  $rowsPerPage = 20;
						  // mengatur variabel reload dan sql
						  $aktif = @$_REQUEST['aktif'];
						  if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
							 // jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)
							 $keyword = htmlspecialchars(@$_REQUEST['keyword']);
							 if(@$aktif == null){
								$reload = "mst-slider.php?pagination=true&keyword=$keyword";
								$sql = "SELECT * FROM WebMstPost WHERE Judul LIKE '%$keyword%' AND IsAktif = '1' ORDER BY KodePost DESC";
							 } else {
							    $reload = "mst-slider.php?pagination=true&keyword=$keyword&aktif=$aktif";
								$sql = "SELECT * FROM WebMstPost WHERE Judul LIKE '%$keyword%' AND (IsAktif = '0' OR IsAktif is null) ORDER BY KodePost DESC";
							 }
							 $result = @sqlsrv_query($dbconnect, $sql, array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
						  } else {
							 // jika tidak ada kata kunci, pencarian pakai ini
							 if(@$aktif == null){
								$reload = "mst-slider.php?pagination=true";
								$sql = "SELECT * FROM WebMstPost WHERE IsAktif = '1' ORDER BY KodePost DESC";
							 } else {
								$reload = "mst-slider.php?pagination=true&aktif=$aktif";
								$sql = "SELECT * FROM WebMstPost WHERE (IsAktif = '0' OR IsAktif is null) ORDER BY KodePost DESC";
							 }
							 $result = @sqlsrv_query($dbconnect, $sql, array(), array( "Scrollable" => 'static' )) or die( print_r( sqlsrv_errors(), true));
						  }
						  
						  require "../library/get-page.php";
						  echo "<tbody>";
						  $pageNum = isset($_GET['pageNum']) ? $_GET['pageNum'] : 1;
						  $no_urut = ($pageNum-1)*$rowsPerPage; 
						  $page = getPage($result, $pageNum, $rowsPerPage);

						  foreach($page as $row){ ?>
						  
                            <tr class="text-left">
                              <td width="5%"><?php echo ++$no_urut; ?></th>
                              <td width="20%" class="text-center">
							  <?php if($row[4] === '' OR $row[4] == null) {
								echo '<img src="../images/posts/thumb_no-image.png" class="img img-responsive img-thumbnail" alt="Image">';
							  } else {
								echo '<img src="../images/posts/thumb_'.$row[4].'" class="img img-responsive img-thumbnail" alt="Image">';
							  }
							  ?>
							  </td>
                              <td><?php echo "<strong>".ucwords($row[2])."</strong><br>".TanggalIndo(DATE_FORMAT($row[1],'Y-m-d'))."<br>Oleh : ".ucwords($row[5])."<br>"; 
								echo '<a href="#uploadImage" data-toggle="modal" data-id="'.htmlspecialchars(base64_encode($row[0])).'" data-gbr="'.htmlspecialchars(base64_encode($row[4])).'">
									  <span class="btn btn-sm btn-warning" title="Ganti Gambar">Ubah Gambar</span></a>'; 
							  ?>
							  </td>
                              <td width="10%"><?php if($row[7] === 1) {
								echo '<a href="mst-post.php?page=_nonaktif&id='.htmlspecialchars(base64_encode($row[0])).'" class="confirm-delete">
										<span class="btn btn-sm btn-success" title="Non Aktifkan">Aktif</span>
									  </a>';  
							  } else {
								echo '<a href="mst-post.php?page=_aktif&id='.htmlspecialchars(base64_encode($row[0])).'" class="confirm-delete">
										<span class="btn btn-sm btn-danger" title="Aktifkan">Non Aktif</span>
									  </a>';   
							  } ?>
							  </td>
							  <?php if(@$_REQUEST['aktif'] == null){
								echo '<td width="15%">';
							  } else {
								echo '<td width="20%">';  
							  } ?>
								<a href="#updateData" data-toggle="modal" data-id="<?php echo htmlspecialchars(base64_encode($row[0])); ?>">
									<span class="btn btn-sm btn-success" title="Edit Data">Edit</span>
								</a>
								<a href="#detailData" data-toggle="modal" data-id="<?php echo htmlspecialchars(base64_encode($row[0])); ?>">
									<span class="btn btn-sm btn-info" title="Lihat Data">View</span>
								</a>
								
								<?php if(@$_REQUEST['aktif'] == null){
									echo '';
								} else {
									echo '<a href="mst-slider.php?page=_delete_slider&id='.htmlspecialchars(base64_encode($row[0])).'" class="confirm-delete">
										  <span class="btn btn-sm btn-danger" title="Hapus Data">Delete</span></a>';
								} 
								?>
							  </td>
                            </tr>
                          
						  <?php } 
						  echo "</tbody>";
						  			
						  $rowsReturned = sqlsrv_num_rows($result);
						  if($rowsReturned === false) {
							 die( print_r( sqlsrv_errors(), true));
						  } elseif($rowsReturned == 0) {
							 echo '<tr class="text-center"><td colspan="5"><br><h5>Tidak Ada Data</h5><br></td></tr>';
							 echo '</table></div>';
						  } else {     
							 $numOfPages = ceil($rowsReturned/$rowsPerPage);
							 echo '</table></div><br>';
							 echo '<div class="text-center">'.paginate_one($reload, $pageNum, $numOfPages).'</div>';
						  } ?>
						
                    </div>
                  </div>
              </div>
            </div>
          </section>
          
          <!-- Page Footer-->
          <?php include "footer.php"; ?>
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../component/vendor/jquery/jquery.min.js"></script>
    <script src="../component/vendor/popper.js/umd/popper.min.js"></script>
    <script src="../component/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../component/vendor/jquery.cookie/jquery.cookie.js"></script>
    <script src="../component/vendor/chart.js/Chart.min.js"></script>
    <script src="../component/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="../component/js/charts-home.js"></script>
    <!-- Main File-->
    <script src="../component/js/front.js"></script>
	
	<!-- No Back Function -->
	<script type="text/javascript">
	window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
	
	<!-- Link to Modal Add and Edit Data -->
	<script type="text/javascript">
    $(document).ready(function(){
        $('#updateData').on('show.bs.modal', function(e) {
			var rowid = $(e.relatedTarget).data('id');
            $.ajax({
				type : 'GET', url : 'update-data.php?page=<?php echo htmlspecialchars(base64_encode("_update_post")); ?>', data : 'rowid='+ rowid,
				success : function(data){
					$('.fetched-data').html(data);
				}
            });
         });
    });
	</script>
	
	<!-- Link to Modal Detail Data -->
	<script type="text/javascript">
    $(document).ready(function(){
        $('#detailData').on('show.bs.modal', function(e) {
			var rowid = $(e.relatedTarget).data('id');
            $.ajax({
				type : 'GET', url : 'detail-data.php?page=<?php echo htmlspecialchars(base64_encode("_detail_post")); ?>', data : 'rowid='+ rowid,
				success : function(data){
					$('.fetched-data').html(data);
				}
            });
         });
    });
	</script>
	
	<!-- Option to Delete Data -->
	<script type="text/javascript">
	jQuery(document).ready(function($){
	$('.confirm-delete').on('click',function(){
        var getLink = $(this).attr('href');
			sweetAlert({
				title: 'Apa Anda Yakin?',
				text: 'Untuk Aktifkan, Non Aktifkan atau Hapus Data Ini',
				type: 'warning',
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			},
			function(){
				window.location.href = getLink
			});
		return false;
		});
	});
	</script>
	
	<!-- Parameter Upload Image -->
	<script type="text/javascript">
	$(document).ready(function(){
		$('#uploadImage').on('show.bs.modal', function(e) {
			var getid = $(e.relatedTarget).data('id');
			var gbrid = $(e.relatedTarget).data('gbr');
			var tmbid = $(e.relatedTarget).data('tmb');
			$(e.currentTarget).find('input[name="_getid"]').val(getid);
			$(e.currentTarget).find('input[name="_gbrid"]').val(gbrid);
			$(e.currentTarget).find('input[name="_tmbid"]').val(tmbid);
		});
	});
	</script>
	
	<!-- Progres Bar Upload Image -->
	<script type="text/javascript" src="../library/upload-image/jquery.form.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() { 
			var progressbox     = $('#_progressbox');
			var progressbar     = $('#_progressbar');
			var statustxt       = $('#_statustxt');
			var completed       = '0%';
			var options = { 
					target: '#_output',   			// target element(s) to be updated with server response 
					beforeSubmit: beforeSubmit,  	// pre-submit callback 
					uploadProgress: OnProgress,
					success: afterSuccess,  		// post-submit callback 
					resetForm: true        			// reset the form after successful submit 
				}; 
				
			 $('#MyUploadForm').submit(function() { 
					$(this).ajaxSubmit(options);  	// return false to prevent standard browser submit and page navigation 
					return false; 
				});
			
		// when upload progresses	
		function OnProgress(event, position, total, percentComplete) {
			// progress bar
			progressbar.width(percentComplete + '%') 	// update progressbar percent complete
			statustxt.html(percentComplete + '%'); 		// update status text
			if(percentComplete>50) {
				statustxt.css('color','#fff'); 		// change status text to white after 50%
			}
		}

		// after succesful upload
		function afterSuccess() {
			$('#_imageInput').hide(); 	// hide input image
			$('#_submit-btn').hide(); 	// hide submit button
			$('#_loading-img').hide(); 	// hide loading 
			$('#_statustxt').hide(); 	// hide status bar 
			$('#_progressbox').hide();  // hide progress
			$('#_oldimg').hide(); 		// hide old image
		}

		// function to check file size before uploading.
		function beforeSubmit() {
			// check whether browser fully supports all File API
			if(window.File && window.FileReader && window.FileList && window.Blob) {
				// check empty input filed
				if(!$('#_imageInput').val()) {	
					sweetAlert("Maaf!", " Masukkan Gambar Terlebih Dahulu ", "error");
					// $("#_output").html("Masukkan gambar terlebih dahulu!");
					return false
				}
				
				var fsize = $('#_imageInput')[0].files[0].size;		// get file size
				var ftype = $('#_imageInput')[0].files[0].type; 	// get file type
				
				// allow only valid image file types 
				switch(ftype){
					case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
						break;
					default:
						sweetAlert("Maaf!", " Tipe File Harus .PNG, .GIF, atau .JPG ", "error");
						// $("#_output").html("<b>"+ftype+"</b> Tipe File Anda Tidak Support!");
						return false
				}
				
				// allowed file size is less than 2 MB (2048576)
				if(fsize>2048576) {
					sweetAlert("Maaf!", " Ukuran File Terlalu Besar ", "error");
					// $("#_output").html("<b>"+bytesToSize(fsize) +"</b> File image terlalu besar! <br/>Perkecil ukuran file terlebih dahulu");
					return false
				}
				
				// progress bar
				progressbox.show(); 			// show progressbar
				progressbar.width(completed); 	// initial value 0% of progressbar
				statustxt.html(completed); 		// set status text
				statustxt.css('color','#000'); 	// initial color of status text
				
				$('#_submit-btn').hide(); 		// hide submit button
				$('#_loading-img').show(); 		// show loading image
				$("#_output").html("");  
				
			}
			else
			{
				// output error to older unsupported browsers that doesn't support HTML5 File API
				sweetAlert("Maaf!", " Browser Anda Tidak Support Fitur Ini ", "error");
				// $("#_output").html("Upgrade browser anda! Browser tidak mendukung fitur ini");
				return false;
			}
		}

		// function to format bites bit.ly/19yoIPO
		function bytesToSize(bytes) {
		   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
		   if (bytes == 0) return '0 Bytes';
		   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
		   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
		}

		}); 

	</script>
	
  </body>
</html>