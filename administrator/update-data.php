<?php 
require "../connections/config.php"; 
date_default_timezone_set('Asia/Jakarta'); 

// get variable POST
$page = isset($_GET['page']) ? base64_decode($_GET['page']) : null ; 
$id = isset($_GET['rowid']) ? base64_decode($_GET['rowid']) : null ; 

?>

<!-- CK Editor -->
<script type="text/javascript" src="../library/ckeditor/ckeditor.js"></script>

<div class="modal-header">
	<?php if($id == null){
		echo '<h4 id="exampleModalLabel" class="modal-title">Tambah Data</h4>';
	} else {
		echo '<h4 id="exampleModalLabel" class="modal-title">Ubah Data</h4>';
	} ?>
	<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span class="fa fa-close"aria-hidden="true"></span></button>
</div>

<!-- Modal Slider -->
<?php if($page === "_update_slider") { 
	if($id == null){
		echo '<form id="myform" method="post" action="mst-slider.php?page=_save_slider&id=">';
	} else {
		echo '<form id="myform" method="post" action="mst-slider.php?page=_save_slider&id='.htmlspecialchars(base64_encode($id)).'">';
	} 
	$query = @sqlsrv_query($dbconnect, "select * from WebMstSlider where KodeSlider = '".$id."'") or die( print_r( sqlsrv_errors(), true));
	while($data = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)) { $judul = $data["Judul"]; $desc = $data["Deskripsi"]; }
?>

<div class="modal-body">
	<div class="form-group">
		<?php if($id == null) { 
			echo '<input type="text" name="_title" placeholder="Judul Slider" class="form-control" autocomplete="off">';
		} else {
			echo '<input type="text" name="_title" placeholder="Judul Slider" value="'.$judul.'" class="form-control" autocomplete="off">';
		} ?>
    </div>
    <div class="form-group">
		<?php if($id == null) { 
			echo '<textarea type="text" name="_descript" placeholder="Masukkan Deskripsi" rows="5" class="form-control"></textarea>';
		} else {
			echo '<textarea type="text" name="_descript" placeholder="Masukkan Deskripsi" rows="5" class="form-control">'.$desc.'</textarea>';
		} ?>
    </div>
</div>

<!-- Modal Post -->
<?php } elseif($page === "_update_post") { 
	if($id == null){
		echo '<form id="myform" method="post" action="mst-post.php?page=_save_post&id=">';
	} else {
		echo '<form id="myform" method="post" action="mst-post.php?page=_save_post&id='.htmlspecialchars(base64_encode($id)).'">';
	} 
	$query = @sqlsrv_query($dbconnect, "select * from WebMstPost where KodePost = '".$id."'") or die( print_r( sqlsrv_errors(), true));
	while($data = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)) { $judul = $data["Judul"]; $desc = $data["Deskripsi"]; $tgl = $data["Tanggal"]; $author = $data["Author"]; }
?>

<div class="modal-body">
	<div class="form-group">
		<?php if($id == null){ 
			echo '<input type="text" id="tanggal1" name="_tanggal" placeholder="Tanggal Post" value="'.date("Y-m-d").'" class="form-control" autocomplete="off">';
		} else { 
			echo '<input type="text" id="tanggal1" name="_tanggal" placeholder="Tanggal Post" value="'.DATE_FORMAT($tgl,'Y-m-d').'" class="form-control" autocomplete="off">';
		} ?>
    </div>
	<div class="form-group">
		<?php if($id == null) { 
			echo '<input type="text" name="_title" placeholder="Judul Post" class="form-control" autocomplete="off">';
		} else {
			echo '<input type="text" name="_title" placeholder="Judul Slider" value="'.$judul.'" class="form-control" autocomplete="off">';
		} ?>
    </div>
	<div class="form-group">
		<?php if($id == null) { 
			echo '<input type="text" name="_author" placeholder="Penulis" class="form-control" autocomplete="off">';
		} else {
			echo '<input type="text" name="_author" placeholder="Judul Slider" value="'.$author.'" class="form-control" autocomplete="off">';
		} ?>
    </div>
    <div class="form-group">
		<?php if($id == null) { 
			echo '<textarea type="text" id="ckeditor" name="_descript" placeholder="Masukkan Deskripsi" rows="5" class="form-control"></textarea>';
		} else {
			echo '<textarea type="text" id="ckeditor" name="_descript" placeholder="Masukkan Deskripsi" rows="5" class="form-control">'.$desc.'</textarea>';
		} ?>
		<script type="text/javascript">
			var editor = CKEDITOR.replace('ckeditor');
			CKFinder.setupCKEditor(editor, 'ckfinder/');    
		</script>
    </div>
</div>

<?php } elseif($page === "_update_galeri") { 
	if($id == null){
		echo '<form id="myform" method="post" action="mst-galeri.php?page=_save_galeri&id=">';
	} else {
		echo '<form id="myform" method="post" action="mst-galeri.php?page=_save_galeri&id='.htmlspecialchars(base64_encode($id)).'">';
	} 
	$query = @sqlsrv_query($dbconnect, "select * from WebMstGaleri where KodeGaleri = '".$id."'") or die( print_r( sqlsrv_errors(), true));
	while($data = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)) { $judul = $data["Judul"]; $desc = $data["Deskripsi"]; }
?>

<div class="modal-body">
	<div class="form-group">
		<?php if($id == null) { 
			echo '<input type="text" name="_title" placeholder="Judul Galeri" class="form-control" autocomplete="off">';
		} else {
			echo '<input type="text" name="_title" placeholder="Judul Galeri" value="'.$judul.'" class="form-control" autocomplete="off">';
		} ?>
    </div>
    <div class="form-group">
		<?php if($id == null) { 
			echo '<textarea type="text" name="_descript" placeholder="Masukkan Deskripsi" rows="5" class="form-control"></textarea>';
		} else {
			echo '<textarea type="text" name="_descript" placeholder="Masukkan Deskripsi" rows="5" class="form-control">'.$desc.'</textarea>';
		} ?>
    </div>
</div>

<?php } elseif($page === "_update_user" OR $page === "_update_userlogin") { 
	if($id == null){
		if($page === "_update_user") {
			echo '<form id="myform" onSubmit="return validasi_person()" method="post" action="mst-user.php?page=_save_user&id=">';
		} elseif($page === "_update_userlogin") {
			echo '<form id="myform" onSubmit="return validasi_person()" method="post" action="ubah-profil.php?page=_save_userlogin&id=">';
		}
	} else {
		if($page === "_update_user") {
			echo '<form id="myform" onSubmit="return validasi_person()" method="post" action="mst-user.php?page=_save_user&id='.htmlspecialchars(base64_encode($id)).'">';
		} elseif($page === "_update_userlogin") {
			echo '<form id="myform" onSubmit="return validasi_person()" method="post" action="ubah-profil.php?page=_save_userlogin&id='.htmlspecialchars(base64_encode($id)).'">';
		}
	} 
	$query = @sqlsrv_query($dbconnect, "select * from WebMstUser where KodeUser = '".$id."'") or die( print_r( sqlsrv_errors(), true));
	while($data = @sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC)) { $nama = $data['NamaUser']; $alamat = $data['Alamat']; $telp = $data['NoTelp']; $email = $data['Email']; }
?>

<div class="modal-body">
	<div class="form-group">
		<?php if($id == null) { 
			echo '<input type="text" name="_nama" placeholder="Nama User" class="form-control" autocomplete="off">';
		} else {
			echo '<input type="text" name="_nama" placeholder="Nama User" value="'.$nama.'" class="form-control" autocomplete="off">';
		} ?>
    </div>
    <div class="form-group">
		<?php if($id == null) { 
			echo '<textarea type="text" name="_alamat" placeholder="Alamat User" rows="5" class="form-control"></textarea>';
		} else {
			echo '<textarea type="text" name="_alamat" placeholder="Alamat User" rows="5" class="form-control">'.$alamat.'</textarea>';
		} ?>
	</div>
	<div class="form-group">
		<?php if($id == null) { 
			echo '<input type="text" id="nomor" name="_telp" placeholder="Nomor Telepon" class="form-control" autocomplete="off">';
		} else {
			echo '<input type="text" id="nomor" name="_telp" placeholder="Nomor Telepon" value="'.$telp.'" class="form-control" autocomplete="off">';
		} ?>
    </div>
	<div class="form-group">
		<?php if($id == null) { 
			echo '<input type="text" id="email" name="_email" placeholder="Email" class="form-control" autocomplete="off">';
		} else {
			echo '<input type="text" id="email" name="_email" placeholder="Email" value="'.$email.'" class="form-control" autocomplete="off">';
		} ?>
    </div>
	<?php if($id == null) { 
		echo '<hr>';
		echo '<div class="form-group"><input type="text" name="_user" placeholder="Username" class="form-control" autocomplete="off"></div>';
		echo '<div class="form-group"><input type="password" name="_password" placeholder="Password" class="form-control" autocomplete="off"></div>';
	} else {
		echo '';
	} ?>
</div>

<?php } ?>

<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-secondary">Keluar</button>
	<button type="submit" class="btn btn-primary">Simpan</button>
    </form>
</div>

<!-- ZebraDatepicker JavaScript -->
<link href="../library/zebra-datepicker/css/default.css" rel="stylesheet" type="text/css">
<script src="../library/zebra-datepicker/javascript/zebra_datepicker.js"></script>
<script src="../library/zebra-datepicker/javascript/zebra_datepicker.src.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	$('#tanggal1').Zebra_DatePicker();
});
</script>

<!-- fungsi untuk validasi form input atau edit -->
<script type="text/javascript">
function validasi_person() {
	var number = /^[0-9]+$/;
	var username = /^[a-zA-Z0-9\_\-]{6,100}$/;
	var mail = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	
	// validasi person
	var nomor = document.forms["myform"]["nomor"].value;
	var email = document.forms["myform"]["email"].value;
		if (!nomor.match(number)) {
			sweetAlert("Maaf!", " Kontak Harus Diisi Angka ", "error");
			return false;
		};
		if (!email.match(mail)) {
			sweetAlert("Maaf!", " Format Email Anda Salah ", "error");
			return false;
		};
}
</script>