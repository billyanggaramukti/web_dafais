<?php
//mengaktifkan session
session_start();
header("Content-type: image/png");

function random($panjang_karakter){  
	$karakter = 'ABCDEFGHJKLMNOPQRSTUVWXYZ1234567890abcdefghijkmnopqrstuvwxyz';  
	$string = '';  
	for($i = 0; $i < $panjang_karakter; $i++) {  
		$pos = rand(0, strlen($karakter)-1);  
		$string .= $karakter{$pos};  
	}  
  return $string;  
}

$text = random(7);
 
// menentukan session
@$_SESSION["_captcha_user"]="";
 
// membuat gambar dengan menentukan ukuran
$gbr = imagecreate(150, 50);

//warna background captcha
imagecolorallocate($gbr, 255, 255, 255);
$captcha_text_color = imagecolorallocate($gbr, 0,0,0);
$captcha_noise_color = imagecolorallocate($gbr, 0,0,0);
$captcha_line_color = imagecolorallocate($gbr, 0,0,0);
 
// pengaturan font captcha
$color = imagecolorallocate($gbr, 0, 0, 0);
$font = "fonts/robotocondensed-regular-webfont.ttf"; 
$ukuran_font = 20;
$posisi = 32;
// membuat nomor acak dan ditampilkan pada gambar
@$_SESSION["_captcha_user"].=$text;
$kemiringan= rand(10, -10);
imagettftext($gbr, $ukuran_font, $kemiringan, 8+15, $posisi, $color, $font, $text);

for($i=0;$i<=5;$i++) {
	imageline($gbr,0,rand()%100,200,rand()%50,$captcha_line_color);
}
for($i=0;$i<100;$i++) {
	imagesetpixel($gbr,rand()%150,rand()%50,$captcha_noise_color);
}
//untuk membuat gambar 
imagepng($gbr); 
imagedestroy($gbr);
?>