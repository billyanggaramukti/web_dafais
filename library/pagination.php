<?php
// deklarasi fungsi paging bootstrap 
function paginate_one($reload, $pageNum, $numOfPages) {
	$firstlabel = "&laquo;&nbsp;";
	$prevlabel  = "&lsaquo;&nbsp;";
	$nextlabel  = "&nbsp;&rsaquo;";
	$lastlabel  = "&nbsp;&raquo;";
	
	$out = "<ul class=\"pagination\">";
	
	// deklarasi tombol first page
	if($pageNum > 1) { $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>"; }
	else { $out.= "<li><span>" . $firstlabel . "</span></li>"; }
	
	// deklarasi tombol previous page
	if($pageNum == 1) { $out.= "<li><span>" . $prevlabel . "</span></li>"; }
	elseif($pageNum==2) { $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>"; }
	else { $out.= "<li><a href=\"" . $reload . "&amp;pageNum=" . ($pageNum-1) . "\">" . $prevlabel . "</a></li>"; }
	
	// deklarasi current page
	$out.= "<li><span class=\"current\">Page " . $pageNum . " of " . $numOfPages . "</span></li>";
		
	// deklarasi tombol next page
	if($pageNum<$numOfPages) { $out.= "<li><a href=\"" . $reload . "&amp;pageNum=" .($pageNum+1) . "\">" . $nextlabel . "</a></li>"; }
	else { $out.= "<li><span>" . $nextlabel . "</span></li>"; }
	
	// deklarasi tombol last page
	if($pageNum<$numOfPages) { $out.= "<li><a href=\"" . $reload . "&amp;pageNum=" . $numOfPages . "\">" . $lastlabel . "</a></li>"; }
	else { $out.= "<li><span>" . $lastlabel . "</span></li>"; }
	
	$out.= "</ul>";
	
	return $out;
}
?>